/*
 *
 * Variables globales
 *
 */

let id = {
  "folderDailyReport01": "1v_dLInTid6Qpy0KY8t7AOE9CX5_seohZ",
  "folderDailyReport02": "171o8BdIl2PwbkvQuwt5k6NiF5p_eSqX0",
  "folderDailyReport03": "1IUYXo_45Wr7z8-10LsqyU5fMVZ_e7GlF",
  "folderDailyReport04": "1pXaK0KiFf6fKG5sl2o-x8hlAqdlU6-Rw",
  "folderDailyReport05": "1MEjWN6qT4ldmwlorxz9IVqPfP5QCfqMx",
  "folderDailyReport06": "1_3odZxG-AN4ehZLhEhNj9-S02cfFQhkW",
  "folderDailyReport07": "14kMaJCcDRFItrcdaCIDxui7g0GF2pPvu",
  "folderDailyReport08": "1nQpdoQxCi1N8dwjYkdzD6JGF41ewEsFE",
  "folderDailyReport09": "19wIdvvKn9mAdYtVvRVFEaKvBXS8Ma3Jh",
  "folderDailyReport10": "1Us-C0kGqY1fJ5CiTH_zkrhY8jeR_KDaf",
  "folderDailyReport11": "1oOApimZNiJPqc2n-URxsSPCzVWBhVqk2",
  "folderDailyReport12": "1qUQjZvDS3T9FLWbsE0elBCks83eZCiZ8",
  "folderProgramaCaja": "11XZlxD4QNOQ8O5rTD27_emfEcHC1mUkI",
  "dailyReportTemplate": "1AWRGbtiGR5Gks5QGrF0UF5dPUyHIog1PMTVIWhhq1zg",
  "pagos": "1vpgvVzAXQf9j_ouTCJIUpcpeyQQN4qkdQdnKwfc2BUI",
  "ingresos": "1D4P85iNO-pto1UhBCSFM02L0lnLR_dzRh1tcYJjae5I",
  "formPagos": "1k__3Hlls4nTQZaKrzldVfqhGGTm6dKz1pHkmxoptHHE",
  "formOrdenes": "1lof500Q7G37MaXvH7cmS1S1tEkezxEd6Auf8lrW9aas",
  "formClientes": "1nK3kMqHryJk_Ql3x3UgM3scHlxYJWuHjwoyfKbcjZHw",
  "folderClientes": "1OR_PGiHS0JkjttrvzcgnCF0W5wiyGqXJ",
  "ssPlantillaClientes": '1OIKHwmsiD11Dm6dN07Vh6Tmd4ndc4xLwun74S4_fsWs'
}

/*
 *
 * Funciones
 *
 */
 
function hola(){
  let contacts = ContactsApp.getContactGroups()
  for (let i in contacts){
    let contact = contacts[i]
    console.log(contact.getName())
  }
}

/*
 * Función principal de form submit.
 */
function formSubmit(e){
  let numEdiciones = setContadorEdiciones(e)
  if (numEdiciones == 1){
    updateForm(e)
    let editURL = setEditResponseUrl(e)
    let ficha = crearFicha(e, editURL)
    setFichaInSs(e, ficha)
    let idGC = addContact(e.namedValues)
    setIdGC(e, idGC)
    sheetStyle(e.range.getSheet())
  }
  else{
    editContact(e)
  }
}

/*
 * Suma 1 al contador de ediciones. Si es nueva se le coloca 1.
 */
function setContadorEdiciones(e){
  let s = e.range.getSheet()
  let row = e.range.getRow()
  let column = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("Contador de ediciones") + 1
  let cell = SpreadsheetApp.getActiveSheet().getRange(row, column)
  if (cell.isBlank()){
    cell.setValue(1)
    return 1
  }
  else{
    let rev = new Number(cell.getValue()) + 1
    cell.setValue(rev.valueOf())
    return rev.valueOf()
  }
}

/*
 * Actualiza a los clientes en las opciones de ingreso de pago
 * Hay que actualizar esto para que haga también con ingreso de ordenes 
 */
function updateForm(e){
  let formPagos = FormApp.openById(id["formPagos"])
  let itemClientesForPagos = formPagos.getItemById("1043650931").asListItem()
  let formOrdenes = FormApp.openById(id["formOrdenes"])
  let itemClientesForOrdenes = formOrdenes.getItemById("1752923801").asListItem()
  let s = SpreadsheetApp.getActiveSheet()
  let values = s.getRange(2, 2, s.getLastRow()-1 , 2).getValues()
  let choicesPagos = []
  let choicesOrdenes = []
  for (let i = 0; i < values.length; i++){
    choicesPagos.push(itemClientesForPagos.createChoice(values[i][0] + " " + values[i][1]))
    choicesOrdenes.push(itemClientesForOrdenes.createChoice(values[i][0] + " " + values[i][1]))
  }
  itemClientesForPagos.setChoices(choicesPagos)
  itemClientesForOrdenes.setChoices(choicesOrdenes)
}

/*
 * Fijar el links de edición. Me basé en el link siguiente pero modificado para que solo lo haga con el que se ingresa.
 * https://gist.github.com/rubenrivera/4ed2110cda3fbdbc29d2d2d3a4af29c0
 */
function setEditResponseUrl(e){
  let s = SpreadsheetApp.getActiveSheet()
  let form = FormApp.openById(id["formClientes"])
  let column = 1 // = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("Marca temporal") + 1
  let row = e.range.getRow()
  let timestamp = s.getRange(row, column).getValue()
  let formSubmitted = form.getResponses(timestamp)
  let editResponseUrl = formSubmitted[0].getEditResponseUrl()
  let text = "Editar a " + formSubmitted[0].getItemResponses()[0].getResponse()
  column = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("Editar cliente") + 1
  s.getRange(row, column).setValue('=HYPERLINK("'+editResponseUrl+'";"'+text+'")')
  return editResponseUrl
}

/*
 * Crea la ficha personal (ss) del cliente. Le asigna nombre al spreadsheet y fija el link para editarlo.
 */
function crearFicha(e, editResponseUrl){
  let values = e.range.getValues()[0]
  let fileSsPlantilla = DriveApp.getFileById(id["ssPlantillaClientes"])
  let folderClientes = DriveApp.getFolderById(id["folderClientes"])
  let name = values[1] + " " + values[2]
  let newFile = fileSsPlantilla.makeCopy(name, folderClientes)
  let s = SpreadsheetApp.open(newFile).getSheetByName("Datos cliente")
  s.getRange(10, 3).setValue('=HYPERLINK("'+editResponseUrl+'";"'+"Editar Cliente"+'")')
  let url = SpreadsheetApp.getActiveSpreadsheet().getUrl()
  let range = e.range
  let row = range.getRow()
  let cell = "Clientes!B"+row
  s.getRange(1, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!C"+row
  s.getRange(2, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!D"+row
  s.getRange(3, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!E"+row
  s.getRange(4, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!F"+row
  s.getRange(5, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!G"+row
  s.getRange(6, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!H"+row
  s.getRange(7, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!I"+row
  s.getRange(8, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!J"+row
  s.getRange(9, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!K"+row
  s.getRange(10, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  cell = "Clientes!L"+row
  s.getRange(11, 2).setValue('=IMPORTRANGE("'+url+'";"'+cell+'")')
  return newFile;
}
  
/*
 * Coloca link de la ficha de detalles del cliente
 */
function setFichaInSs(e, ficha){
  let rango = e.range
  let row = rango.getRow()
  let s = SpreadsheetApp.getActiveSheet()
  let column = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("Ver detalles del cliente") + 1
  s.getRange(row, column).setValue('=HYPERLINK("'+ficha.getUrl()+'";"Ver detalles de '+e.namedValues["Nombre(s)"]+'")')
}

/*
 * Agrega al cliente en la lista de Google Contacts -> celulares -> wsp!
 */
function addContact(values){
  let nombre = values["Nombre(s)"][0]
  let apellido = values["Apellido(s)"][0]
  let correo = values["Correo electrónico"][0]
  let groupClientes = ContactsApp.getContactGroup("Clientes")
  let groupAll = ContactsApp.getContactGroup("System Group: My Contacts")
  let contact = ContactsApp.createContact(nombre, apellido, correo)
  Utilities.sleep(3000)
  
  if (!correo){ contact.addEmail("Personal", "No definido") }
  let telefono = values["Número de teléfono"][0] ? values["Número de teléfono"][0] : "No definido"
  console.log(telefono)
  let empresa = values["Empresa"][0] ? values["Empresa"][0] : "No definido"
  console.log(empresa)
  let oficio = values["Oficio / Puesto"][0] ? values["Oficio / Puesto"][0] : "No definido"
  console.log(oficio)
  let direccion = values["Dirección física"][0] ? values["Dirección física"][0] : "No definido"
  console.log(direccion)
  let factura = values["¿Se le factura?"][0] ? values["¿Se le factura?"][0] : "No definido"
  console.log(factura)
  let rut = values["RUT"][0] ? values["RUT"][0] : "No definido"
  console.log(rut)
  let direccionFact = values["Dirección de facturación"][0] ? values["Dirección de facturación"][0] : "No definido"
  console.log(direccionFact)
  let razonSoc = values["Razón social"][0] ? values["Razón social"][0] : "No definido"
  console.log(razonSoc)

  contact.addCompany(empresa, oficio)
  contact.addAddress("Principal", direccion)
  contact.addPhone("Principal", telefono)
  contact.addCustomField("Se le factura", factura)
  contact.addCustomField("RUT", rut)
  contact.addAddress("Facturación", direccionFact)
  contact.addCustomField("Razón social", razonSoc)
  
  groupClientes.addContact(contact)
  groupAll.addContact(contact)
  
  let id = contact.getId()
  return id.substr(id.lastIndexOf("/") + 1)
}

function editContact(e){
  let s = e.range.getSheet()
  let row = e.range.getRow()
  let headers = s.getRange(1,1,1,s.getLastColumn()).getValues()[0]
  let values = e.namedValues
  
  let nameSs = s.getRange(row, headers.indexOf("Nombre(s)") + 1).getValue()
  let nameForm = values["Nombre(s)"][0]
  let apellidoSs = s.getRange(row, headers.indexOf("Apellido(s)") + 1).getValue()
  let apellidoForm = values["Apellido(s)"][0]
  let correoSs = s.getRange(row, headers.indexOf("Correo electrónico") + 1).getValue()
  let correoForm = values["Correo electrónico"][0]
  let telefonoSs = s.getRange(row, headers.indexOf("Número de teléfono") + 1).getValue()
  let telefonoForm = values["Número de teléfono"][0]
  let empresaSs = s.getRange(row, headers.indexOf("Empresa") + 1).getValue()
  let empresaForm = values["Empresa"][0]
  let oficioSs = s.getRange(row, headers.indexOf("Oficio / Puesto") + 1).getValue()
  let oficioForm = values["Oficio / Puesto"][0]
  let direccionSs = s.getRange(row, headers.indexOf("Dirección física") + 1).getValue()
  let direccionForm = values["Dirección física"][0]
  let facturaSs = s.getRange(row, headers.indexOf("¿Se le factura?") + 1).getValue()
  let facturaForm = values["¿Se le factura?"][0]
  let rutSs = s.getRange(row, headers.indexOf("RUT") + 1).getValue()
  let rutForm = values["RUT"][0]
  let direccionFactSs = s.getRange(row, headers.indexOf("Dirección de facturación") + 1).getValue()
  let direccionFactForm = values["Dirección de facturación"][0]
  let razonSocSs = s.getRange(row, headers.indexOf("Razón social") + 1).getValue()
  let razonSocForm = values["Razón social"][0]
  
  let nombre = nameForm ? nameForm : (nameSs ? nameSs : "No definido")
  let apellido = apellidoForm ? apellidoForm : (apellidoSs ? apellidoSs : "No definido")
  let correo = correoForm ? correoForm : (correoSs ? correoSs : "No definido")
  let telefono = telefonoForm ? telefonoForm : (telefonoSs ? telefonoSs : "No definido")
  let empresa = empresaForm ? empresaForm : (empresaSs ? empresaSs : "No definido")
  let oficio = oficioForm ? oficioForm : (oficioSs ? oficioSs : "No definido")
  let direccion = direccionForm ? direccionForm : (direccionSs ? direccionSs : "No definido")
  let factura = facturaForm ? facturaForm : (facturaSs ? facturaSs : "No definido")
  let rut = rutForm ? rutForm : (rutSs ? rutSs : "No definido")
  let direccionFact = direccionFactForm ? direccionFactForm : (direccionFactSs ? direccionFactSs : "No definido")
  let razonSoc = razonSocForm ? razonSocForm : (razonSocSs ? razonSocSs : "No definido")
  console.log(nombre)
  console.log(apellido)
  console.log(correo)
  console.log(telefono)
  console.log(empresa)
  console.log(oficio)
  console.log(direccion)
  console.log(factura)
  console.log(rut)
  console.log(direccionFact)
  console.log(razonSoc)
  
  let idSs = s.getRange(row, headers.indexOf("Id") + 1).getValue()
  let contacts = ContactsApp.getContacts()
  let contact;
  let id;
  for (var i in contacts){
    contact = contacts[i]
    id = contact.getId()
    id = id.substr(id.lastIndexOf("/") + 1)
    if (id === idSs){
      break
    }
  }
  
  if (id === idSs){
    console.log("entré!")
    contact.setGivenName(nombre).setFamilyName(apellido)
    contact.getEmails()[0].setAddress(correo)   
    contact.getCompanies()[0].setCompanyName(empresa)
    contact.getCompanies()[0].setJobTitle(oficio)
    contact.getAddresses("Principal")[0].setAddress(direccion)
    contact.getPhones()[0].setPhoneNumber(telefono)
    contact.getCustomFields("Se le factura")[0].setValue(factura)
    contact.getCustomFields("RUT")[0].setValue(rut)
    contact.getAddresses("Facturación")[0].setAddress(direccionFact)
    contact.getCustomFields("Razón social")[0].setValue(razonSoc)
  }
  else{
    console.log("Problemas con match el id del cliente!")
    return
  }
}

/*
 * Fija el id de google contacts en la hoja de clientes. Esto para que pueda saber que cliente es al editarlo en el futuro.
 */
function setIdGC(e, id){
  let row = e.range.getRow()
  let s = e.range.getSheet()
  let column = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("Id") + 1
  s.getRange(row, column).setValue(id)
}

/*
 * Darle estilo a la hoja: grid solo arriba y abajo, centrar todo y fijar el alto de las filas.
 */
function sheetStyle(s) {
  let qtyRows = s.getLastRow()-1;
  s.setRowHeights(2, qtyRows, 40)
  let column = s.getRange(1,1,1,s.getLastColumn()).getValues()[0].indexOf("RUT") + 1
  s.getRange(2, column, s.getLastRow(), 1).setNumberFormat('#######-#')
  
  cell = s.getRange(2, 1, s.getLastRow(), s.getLastColumn());
  cell.setFontSize(12).setHorizontalAlignment("center").setVerticalAlignment("middle")
  .setBorder(true, true, true, true, false, true).setWrap(true);
}

function compareArray(ar1,ar2){
    if (ar1.length === ar2.length){
        for (var i in ar1){
            if (ar1[i] !== ar2[i]){
                areEqual = false
            }
        }
    }
    else{
        return false
    }
    return areEqual
}

