function test(){
  console.log(typeof SpreadsheetApp.getActiveSheet().getRange(2, 1).getValue().getMonth)
}


let id = {
  "Plantilla imprimible cliente": "1lFNYSQWLFiDwAV-iGFfzjhkw_iiGiz0oAge07P779Gg",
  "Folder archivos temporales": "1OEcQkKX4NvBiBZyT5V22v4ZhanpsSeIh"
}

function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Generar documento')
      .addItem('Ficha completa del cliente', 'todo')
      .addSeparator()
      .addItem('Para envio', 'paraEnvio')
      .addToUi();
}

function todo() {
  //Hojas de Ss para los datos
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sDatos = ss.getSheetByName("Datos cliente")
  const sOrdenes = ss.getSheetByName("Ordenes")
  const sPagos = ss.getSheetByName("Pagos")
  
  //Datos a usar
  const date = new Date()
  const fecha = tituloBuild(date)
  const hora = date.getHours() + ":" + date.getMinutes()
  const name = sDatos.getRange(1,2).getValue()
  const lastName = sDatos.getRange(2,2).getValue()
  const nombre = name.slice(0,name.indexOf(" ")) + " " + lastName.slice(0,lastName.indexOf(" "))
  const empresa = sDatos.getRange(3,2).getValue() ? sDatos.getRange(3,2).getValue(): "Empresa no ingresada"
  const puesto = sDatos.getRange(4,2).getValue() ? sDatos.getRange(4,2).getValue(): "Puesto no ingresado"
  const telefono = sDatos.getRange(5,2).getValue() ? formatPhone(sDatos.getRange(5,2).getValue()) : "Telefono no ingresado"
  const correo = sDatos.getRange(6,2).getValue() ? sDatos.getRange(6,2).getValue() : "Correo no ingresado"
  const direccion = sDatos.getRange(7,2).getValue() ? sDatos.getRange(7,2).getValue() : "Dirección no ingresada"
  const rut = sDatos.getRange(9,2).getValue() ? formatRut(sDatos.getRange(9,2).getValue()) : "Rut no ingresado"
  const isDeuda = sDatos.getRange(2,3).getValue() ? true : false
  const deudaCredito = isDeuda ? "una deuda de $" + formatNumber(sDatos.getRange(2,3).getValue()) : 
                               "un crédito de $" + formatNumber(sDatos.getRange(4,3).getValue())
  const totalPagos = formatNumber(sDatos.getRange(6,3).getValue())
  const totalOrdenes = formatNumber(sDatos.getRange(8,3).getValue())
  const pagos = sPagos.getRange(2, 1, sPagos.getLastRow() - 1, sPagos.getLastColumn() - 3).getValues()
  const ordenes = sOrdenes.getRange(2, 1, sOrdenes.getLastRow() - 1, sOrdenes.getLastColumn() - 2).getValues()
  
  //Crear archivo
  const doc = DocumentApp.create(nombre + " " + date.toLocaleDateString())
  const file = DriveApp.getFileById(doc.getId())
  DriveApp.getFolderById(id["Folder archivos temporales"]).addFile(file)
  DriveApp.getRootFolder().removeFile(file)
  
  //Estilos a usar
  const bold = {}
  bold[DocumentApp.Attribute.BOLD] = true
  const notBold = {}
  notBold[DocumentApp.Attribute.BOLD] = false
  const italic = {}
  italic[DocumentApp.Attribute.ITALIC] = true
  const right = {}
  right[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] = DocumentApp.HorizontalAlignment.RIGHT
  const top = {}
  top[DocumentApp.Attribute.VERTICAL_ALIGNMENT] = DocumentApp.VerticalAlignment.BOTTOM
  
  //get header, body y footer
  const header = doc.getHeader() ? doc.getHeader() : doc.addHeader()
  const footer = doc.getFooter() ? doc.getFooter() : doc.addFooter()
  const body = doc.getBody()
  
  //Footer
  const divider = footer.appendHorizontalRule()
  const footerText = footer.appendParagraph("Este documento fue generado el " + fecha + " a las " + hora)
  footerText.setAlignment(DocumentApp.HorizontalAlignment.RIGHT)
  
  //Body: Table de datos de la empresa y del cliente. Ordenes. Pagos.
  const bodyTable = body.appendTable().setBorderColor("#FFFFFF")
  const tr = bodyTable.appendTableRow()
  const tc1 = tr.appendTableCell(nombre + ", de " + empresa).setAttributes(top)
  tc1.getChild(0).asParagraph().setAttributes(bold).setAttributes(italic)
  tc1.appendParagraph(rut).setAttributes(notBold)
  tc1.appendParagraph(telefono)
  tc1.appendParagraph(correo)
  tc1.appendParagraph(direccion)
  const tc2 = tr.appendTableCell("Okima Impresiones SpA").setAttributes(top)
  tc2.getChild(0).asParagraph().setAttributes(right).setAttributes(italic).setAttributes(bold)
  tc2.appendParagraph("76968756-4").setAttributes(right).setAttributes(italic).setAttributes(notBold)
  tc2.appendParagraph(formatPhone("226668988")).setAttributes(right).setAttributes(italic)
  tc2.appendParagraph("ventas@okima.cl").setAttributes(right).setAttributes(italic)
  tc2.appendParagraph("Gran Avda. #6113, San Miguel").setAttributes(right).setAttributes(italic)
  body.appendHorizontalRule()
  body.getChild(2).removeFromParent()
  
  body.appendParagraph(nombre + " tiene " + deudaCredito + 
                      ". El total de las ordenes que ha solicitado es $" + totalOrdenes +
                      ". El total de los pagos que nos ha hecho es $" + totalPagos + ".")
      .setHeading(DocumentApp.ParagraphHeading.HEADING1)
      .setAlignment(DocumentApp.HorizontalAlignment.CENTER)
  body.appendParagraph("Órdenes solicitadas").setHeading(DocumentApp.ParagraphHeading.HEADING2)
  for (let i = 0 ; i < ordenes.length ; i++){
    let orden = ordenes[i]
    let fechaOrden = tituloBuild(new Date(orden[0]))
    body.appendListItem("Fecha: " + fechaOrden + "  ||  N° de orden: " + orden[1])
    body.appendListItem(orden[2])
        .setNestingLevel(1)
        .setGlyphType(DocumentApp.GlyphType.BULLET)
    body.appendListItem("$" + formatNumber(orden[3]))
        .setNestingLevel(1)
        .setGlyphType(DocumentApp.GlyphType.BULLET)
        .setLineSpacing(2)
  }

  body.appendParagraph("Pagos realizados").setHeading(DocumentApp.ParagraphHeading.HEADING2)
  for (let i = 0 ; i < pagos.length ; i++){
    let pago = pagos[i]
    let fechaPago = tituloBuild(new Date(pago[0]))
    body.appendListItem("Monto: $" + formatNumber(pago[2]) + "  ||  " + 
                        "Fecha: " + fechaPago + "  ||  " + 
                        "Método: " + pago[1]).setLineSpacing(2)
  }
  body.getChild(0).asText().setFontSize(1)
  
  const url = doc.getId()
  doc.saveAndClose()
  const pdf = gdocToPDF(url)
  if (correo != "Correo no ingresado"){
    const ui = SpreadsheetApp.getUi()
    const response = ui.alert('Ya se creó el documento. ¿Quieres enviar el pdf al cliente?', ui.ButtonSet.YES_NO)
    if (response == ui.Button.YES) {
      
      const response = ui.prompt('¡Dile quien eres al cliente!', 'Escribe tu nombre', ui.ButtonSet.OK_CANCEL)
      if (response.getSelectedButton() == ui.Button.OK) {
        var contentHTML = "<body> <font color='#0098c0'> <b>Okima Impresiones SpA - " + response.getResponseText() + 
            "</b> </font> \n \n <p> Estimad@ " + nombre + 
            ", le invitamos a revisar su <b>ficha de ordenes y pagos en el archivo adjunto.</b></p> <br>" + 
            "<p> Saludos! </p> </body>" + 
            '<table style="font-size:medium;font-family:Arial;min-width:450px" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr style="text-align:center"><td><h3 color="#3b3b3b" style="margin:0;font-size:18px;color:#3b3b3b" class="sc-fBuWsC eeihxG"><span>Tac</span><span>&nbsp;</span><span>Yon</span></h3><p color="#3b3b3b" font-size="medium" style="margin:0;color:#3b3b3b;font-size:14px;line-height:22px" class="sc-fMiknA bxZCMx"><span>Desarrollador</span></p><p color="#3b3b3b" font-size="medium" style="margin:0;font-weight:500;color:#3b3b3b;font-size:14px;line-height:22px" class="sc-dVhcbM fghLuF"><span>Okima Impresiones SpA</span></p></td></tr><tr><td><table style="font-size:medium;font-family:Arial;width:100%" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td height="30"></td></tr><tr><td color="#0098c0" direction="horizontal" style="width:100%;border-bottom:1px solid #0098c0;border-left:medium none;display:block" class="sc-jhAzac hmXDXQ" height="1"></td></tr><tr><td height="30"></td></tr></tbody></table><table style="font-size:medium;font-family:Arial;width:100%" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr style="vertical-align:middle"><td><table style="font-size:medium;font-family:Arial" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr style="vertical-align:middle" height="25"><td style="vertical-align:middle" width="30"><table style="font-size:medium;font-family:Arial" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td style="vertical-align:bottom"><span color="#0098c0" width="11" style="display:block;background-color:#0098c0" class="sc-jlyJG bbyJzT"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/phone-icon-2x.png" color="#0098c0" style="display:block;background-color:#0098c0" class="sc-iRbamj blSEcj" width="13"></span></td></tr></tbody></table></td><td style="padding:0;color:#3b3b3b"><a href="tel:+56 2 2666 8988" color="#3b3b3b" style="text-decoration:none;color:#3b3b3b;font-size:12px" class="sc-gipzik iyhjGb"><span>+56 2 2666 8988</span></a> | <a href="https://wa.me/56966702747" color="#3b3b3b" style="text-decoration:none;color:#3b3b3b;font-size:12px" class="sc-gipzik iyhjGb"><span>+56 9 6670 2747</span></a></td></tr><tr style="vertical-align:middle" height="25"><td style="vertical-align:middle" width="30"><table style="font-size:medium;font-family:Arial" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td style="vertical-align:bottom"><span color="#0098c0" width="11" style="display:block;background-color:#0098c0" class="sc-jlyJG bbyJzT"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/email-icon-2x.png" color="#0098c0" style="display:block;background-color:#0098c0" class="sc-iRbamj blSEcj" width="13"></span></td></tr></tbody></table></td><td style="padding:0"><a href="mailto:ventas@okima.cl" color="#3b3b3b" style="text-decoration:none;color:#3b3b3b;font-size:12px" class="sc-gipzik iyhjGb"><span>ventas@okima.cl</span></a></td></tr><tr style="vertical-align:middle" height="25"><td style="vertical-align:middle" width="30"><table style="font-size:medium;font-family:Arial" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td style="vertical-align:bottom"><span color="#0098c0" width="11" style="display:block;background-color:#0098c0" class="sc-jlyJG bbyJzT"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/link-icon-2x.png" color="#0098c0" style="display:block;background-color:#0098c0" class="sc-iRbamj blSEcj" width="13"></span></td></tr></tbody></table></td><td style="padding:0"><a href="//www.okima.cl" color="#3b3b3b" style="text-decoration:none;color:#3b3b3b;font-size:12px" class="sc-gipzik iyhjGb"><span>www.okima.cl</span></a></td></tr><tr style="vertical-align:middle" height="25"><a href="https://goo.gl/maps/S1qougiQg4rn2fbu9"><td style="vertical-align:middle" width="30"><table style="font-size:medium;font-family:Arial" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td style="vertical-align:bottom"><span color="#0098c0" width="11" style="display:block;background-color:#0098c0" class="sc-jlyJG bbyJzT"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/address-icon-2x.png" color="#0098c0" style="display:block;background-color:#0098c0" class="sc-iRbamj blSEcj" width="13"></span></td></tr></tbody></table></td><td style="padding:0"><span color="#3b3b3b" style="font-size:12px;color:#3b3b3b" class="sc-csuQGl CQhxV"><span>Gran Avenida 6113, San Miguel</span></span></td></a></tr></tbody></table></td></tr></tbody></table></td><td width="15"><div></div></td><td style="text-align:right"><table style="font-size:medium;font-family:Arial;width:100%" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td><img src="https://www.okima.cl/img/logo.png" role="presentation" style="max-width:130px;display:inline-block" class="sc-cHGsZl bHiaRe" width="130"></td></tr><tr><td height="10"></td></tr><tr><td><table style="font-size:medium;font-family:Arial;display:inline-block" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr style="text-align:right"><td><a href="//www.facebook.com/okima.impresiones" color="#0098c0" style="display:inline-block;padding:0;background-color:#0098c0" class="sc-hzDkRC kpsoyz"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/facebook-icon-2x.png" alt="facebook" color="#0098c0" style="background-color:#0098c0;max-width:135px;display:block" class="sc-bRBYWo ccSRck" height="24"></a></td><td width="5"><div></div></td><td><a href="//www.instagram.com/okima.impresiones" color="#0098c0" style="display:inline-block;padding:0;background-color:#0098c0" class="sc-hzDkRC kpsoyz"><img src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/instagram-icon-2x.png" alt="instagram" color="#0098c0" style="background-color:#0098c0;max-width:135px;display:block" class="sc-bRBYWo ccSRck" height="24"></a></td><td width="5"><div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table style="font-size:medium;font-family:Arial;width:100%" class="sc-gPEVay eQYmiW" cellspacing="0" cellpadding="0"><tbody><tr><td height="30"></td></tr><tr><td color="#0098c0" direction="horizontal" style="width:100%;border-bottom:1px solid #0098c0;border-left:medium none;display:block" class="sc-jhAzac hmXDXQ" height="1"></td></tr><tr><td height="30"></td></tr></tbody></table><tr><td style="text-align:center"><span style="display:block;text-align:center"><a target="_blank" rel="noopener noreferrer" href="https://www.okima.cl/catalogoOkima.pdf" color="#0098c0" style="border-color:#0098c0;border-style:solid;border-width:6px 12px;display:inline-block;background-color:#0098c0;color:#fff;font-weight:700;text-decoration:none;text-align:center;line-height:40px;font-size:12px;border-radius:3px" class="sc-fAjcbJ byigni">Revisa nuestro catalogo</a></span></td></tr>'
        GmailApp.sendEmail(correo, "Informe - Okima Impresiones", "Estimad@ " + nombre + 
        ", le adjunto un informe con sus ordenes y pagos. Atentamente " + response.getResponseText(), {
          attachments: [pdf.getAs('application/pdf')],
          htmlBody: contentHTML,
          name: 'Okima Impresiones - ' + response.getResponseText()
        })
      }
      else{ui.alert('Cancelaste el envío del documento')}
    }
    else{ui.alert('Cancelaste el envío del documento')}
  }
}

function gdocToPDF(fileID) {
  const folder = DriveApp.getFolderById(id["Folder archivos temporales"]) 
  const file = DriveApp.getFileById(fileID);
  const theBlob = file.getBlob().getAs('application/pdf');
  const newPDFFile = folder.createFile(theBlob);
  const fileName = file.getName().replace(".", "");
  newPDFFile.setName(fileName + ".pdf");
  folder.removeFile(file)
  return newPDFFile
}

function sendFile(file, correo, mensaje, nombreRemitente){
  GmailApp.sendEmail(correo, "Informe - Okima Impresiones", mensaje, {
     attachments: [file.getAs('application/pdf')],
     name: 'Okima Impresiones - ' + nombreRemitente
  })
}

function formatNumber(n) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

function formatPhone(n) {
  n = n.toString()
  return n[0] + " " + n.slice(1,5) + " " + n.slice(5,9)
}

function formatRut(n){
  n = n.toString()
  return n.substr(0,n.length-1) + "-" + n[n.length - 1]
}

function tituloBuild(date){
  let dia = new String(date.getDate());
  let mes = new String(date.getMonth() + 1);
  let ano = new String(date.getFullYear());
  if (dia.length === 1) { dia = new String('0' + dia); }
  if (mes.length === 1) { mes = new String('0' + mes); }
  let fecha = dia + '-' + mes + '-' + ano;
  let diaSemana = diasWeek[new Date(ano+'/'+mes+'/'+dia).getDay()]
  return fecha + ', ' + diaSemana;
}

