/**
 * Creates a trigger for when spreadsheet Ordenes recibe una entrada desde su formulario
 */
function triggerForSsPagos() {
  const ss = SpreadsheetApp.openById(idSSheets['ordenes']);
  ScriptApp.newTrigger('formSubmitPagosOrdenes')
    .forSpreadsheet(ss)
    .onFormSubmit()
    .create()
}
/**
 * Creates a trigger for when a spreadsheet opens.
 */
function triggerForSsOrdenes() {
  const ss = SpreadsheetApp.openById(idSSheets['pagos']);
  ScriptApp.newTrigger('formSubmitPagosOrdenes')
    .forSpreadsheet(ss)
    .onFormSubmit()
    .create()
}