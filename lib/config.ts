const diasWeek = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
const months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
const pluralToSingular = {
  clientes: 'cliente',
  pagos: 'pago',
  ordenes: 'orden',
  proveedores: 'proveedor',
  compras: 'compra',
}
const idFolder = {
  reports: '10DgcYHf-D7KOqjNMVK1pezHsh0QJz--e',
  
  clientes: '1G4xXkIgQ3mOd1XHqGi9huV3XCy5R2aSL',
  pagos: '1lvP7z86HwqfmgVhi7S-hacxsfz_G4ohj',
  ordenes: '18N5K68kT4RArEvWkBaUI6fv9YjxYWSkZ',
  proveedores: '1je-_EakpmY3PQyonLkYEpq3qrYduAkdU',
  compras: '1DCRCo_sLvzb9bTP9z1wy0bQqZm5byJNM',
}
const idForms = {
  clientes: '1XdIaNh5fYkJL1bD40KkI637WJ9cuGaxxq3iwUHxnR-E',
  pagos: '1k6Hmcp6uN4uca8plII5oB5t81_xqVhYzBsEtH3vfu8Y',
  ordenes: '1WIqK4Mmy4qdtRUOnJJXnkr3aT6DgEoltZTSuaEYPn54',
  proveedores: '115rJM55t5C19qtUKQR6EdiBNXdUMPq6ieaa1nOqHP7w',
  compras: '1q7OfaU22sN_yKg09R58ZmwAbBc5V-knXjEIwUwYZ1UY',
  empleados: '1c685iq_O3cghs8GJ7XZJfpC28KAnzD30icKjTyoB2Qk',

  compraProveedor: '1mOOeJYLfVYdqCSPvm7gGHIhOqMmtlPiaWNlzQKpUvRs',
  ordenPagoCliente: '10yy5IeTSVIAZMmu7uE3tLLgEX9ds4N1f5hqCxsg_Txs',
}
const idQuestions = {//faltan todas las preguntas
  formPagosCliente: 0,
  formOrdenesCliente: 0,
  formComprasProveedor: 0,

  formOrdenes2Cliente: 0,
  formCompras2Proveedor: 0,

  formPagosEmpleado: 1191483087,
  formOrdenesEmpleado: 392912607,
  formClienteEmpleado: 2061593612,
  formComprasEmpleado: 268331541,
  formProveedorEmpleado: 130793869,
  formEmpleadoEmpleado: 2061593612,

  formOrdenes2Empleado: 0,
  formCompras2Empleado: 0,
}
const idSSheets = { //ok
  pagosAnual: '1vpgvVzAXQf9j_ouTCJIUpcpeyQQN4qkdQdnKwfc2BUI',
  ingresosAnual: '1D4P85iNO-pto1UhBCSFM02L0lnLR_dzRh1tcYJjae5I',
  pagosAnualOld: '1MtoXWJlD86KCB4zKD8hpHa0gxuL2APPn_dguYZOsthY',
  balanceReportTemplate: '1YIhDiwijT-wgH4iL66Kzq8xzj-iuYNCjGNh7GbdRHho',

  clienteTemplate: '1w9eXeRz_80ZOHb3uBE3L4l5bwsSIAI2UUZeljNl404U',
  pagosTemplate: '1hRZ-Dwg5maN3Nr2nFOVRkH2dPzcx2xwSzbCjbOJjh7M',
  ordenesTemplate: '10KUY43XSzvyJDPgJjJ5ev0TrpYdb3KUi_VWvhGvi5Ds',
  proveedoresTemplate: '1HNwHkZd7jxvIrx49fRFJ9e5pjS4lxG-apH03knpABj4',
  comprasTemplate: '1s8teBRCiPstkXWvCR_aSKz2D-HccTALhq8zSHk17XHc',
  empleadoTemplate: '1fbWVnivLzmQwhRTk7Z06A4UmGN_zPDPuY7pKZANvL2g',

  clientes: '1jYgjOZCRQcc4tqwue6UEkeW1fkaSXVz4ugswh2U9XRg',
  pagos: '1w7gBaThoyVCg4dqjWeiskukGj32-XHI_077KRkdvAKk',
  ordenes: '1yfr4dTOJK6jfda0dlZXXHxODiLAxo3WKAAEpgwFvQLs',
  proveedores: '1IMtJYCNsSrOcSL5cKbUDpezO1Kr-BrtUBOSAstHQsjg',
  compras: '1A3CWGDHiL3hI3Ik0ToNmVUMwfogJ_RmWLxt-47gIG6M',
  empleados: '1FIqB9SQvIpSHrZsg1yVQ5FnEYzNyv3Ovn949KLyWhDs',
}
const formatForCells = {
  date: 'dddd, d"/"m"/"yy',
  clp: '[$$]#,##0',
  phone: '# #### ####',
  rut: '#-#',
}
/**
 * Fija las propiedeades del script, según el objeto que se le entregue.
 * @param {{}} properties 
 */
function setProperties(properties){
  const scriptProperties = PropertiesService.getScriptProperties()
  const keys = scriptProperties.getKeys()
  let found = false;
  for (let key in properties){
    for (let i = 0; i < keys.length; i++){
      if (key == keys[i]) found = true 
    }
    if (!found){
      scriptProperties.setProperty(key, properties[key])
    }
    found = false
  }
}
const properties = {
  clientes: -1,
  pagos: -1,
  ordenes: -1,
  proveedores: -1,
  compras: -1,
  productos: -1
}

/**
 * ID ES EL timestamp o Marca Temporal
 * e.range.getCell(1,1) (Marca temporal): Date object with time
 * e.range.getCell(1,3) (Fecha): Date object without time (00:00:00)
 * NO USAR e.namedValues["Marca temporal"][0]: String with this format: 5/04/2020 18:20:06 (Months always 2 digit. Day 1 or 2 digit)
 * NO USAR e.namedValues["Fecha"][0]: String with this format: 5/04/2020 (Months always 2 digit. Day 1 or 2 digit)
 */ 
