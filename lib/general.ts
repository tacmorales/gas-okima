/**
 * Fija el contador de ediciones en la entrada que se entrega
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que representa la entrada
 */
function setEditionsCount(range: GoogleAppsScript.Spreadsheet.Range): number {
  const sheet = range.getSheet()
  const cell = sheet.getRange(range.getRow(), getColumnByHeader(sheet, "Contador de ediciones"))
  if (cell.isBlank()) {
    cell.setValue(0)
    return 0
  } else {
    const conteo = Number(cell.getValue()) + 1
    cell.setValue(conteo)
    return conteo
  }
}
/**
 * Fija la url para editar la entrada que se entrega. Se ocupa el timestamp para encontrar la respuesta y así la url. https://gist.github.com/rubenrivera/4ed2110cda3fbdbc29d2d2d3a4af29c0
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que representa la entrada
 * @param {Date} timestamp Fecha que está en la sheet, viene directamente en Date object
 * @param {string} nameEntry Nombre de la entrada, para escribir: Editar pago, o Editar {nameCliente}
 */
function setEditUrl(range: GoogleAppsScript.Spreadsheet.Range, timestamp: Date, nameEntry: string): string {
  const sheet = range.getSheet()
  const editResponseUrl = FormApp.openByUrl(sheet.getFormUrl()).getResponses(timestamp)[0].getEditResponseUrl()
  const value = '=HYPERLINK("' + editResponseUrl + '";"Editar ' + nameEntry + '")'
  setValueInEntry(range, 'Editar', value)
  return editResponseUrl
}
/**
 * Le da style general al rango que se le entrega. Además de darle formato de número (fecha y clp)
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que representa la entrada en la que se encuentra el valor que se desea.
 * @param {string} columnName Nombre de la columna en la cual se quiere recuperar un valor. 
 */
function getOldValue(range: GoogleAppsScript.Spreadsheet.Range, columnName: string): string {
  const sheet = range.getSheet()
  const column = getColumnByHeader(sheet, columnName)
  const text = 'Último: '
  const cell = sheet.getRange(range.getRow(), column)
  let note = cell.getNote()
  const value = cell.getValue().toString();
  note = (note.substr(note.indexOf(text) + text.length)).substr(0, value.length)
  return note
}
/** AGREGAR LA CAPACIDAD DE QUE SE PUEDA COLOCAR ARRAYS Y OBJETOS Y ESCRIBIR UN GET
 * Coloca un valor adicional (o edita) un valor de la fila que indica el rango, y la columna con el nombre indicad
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que representa la entrada
 * @param {string} columnName Nombre de la columna
 * @param {primitives} value Valor que se coloca en la celda definida 
 */
function setValueInEntry(range: GoogleAppsScript.Spreadsheet.Range, columnName: string, value: any) {
  const sheet = range.getSheet()
  const column = getColumnByHeader(sheet, columnName)
  sheet.getRange(range.getRow(), column).setValue(value)
}
/** 
 * Busca y retorna el archivo con el nombre name en la carpeta folder
 * @param {string} name Nombre del archivo
 * @param {GoogleAppsScript.Drive.Folder} folder Carpeta en la cual buscar
 */
function getFileByName(name: string, folder: GoogleAppsScript.Drive.Folder): GoogleAppsScript.Drive.File {
  const files = folder.getFilesByName(name)
  const file = files.next()
  if (!file) {
    console.log('Archivo con el nombre ' + name + ' no existe')
    return
  }
  if (files.hasNext()) {
    console.log('Archivo con el nombre ' + name + ' no es único')
    return
  }
  return file
}
/**
 * Retorna un number que representa el número de la columna que se indica según el columnName
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet Hoja que contiene el header
 * @param {string} columnName Nombre de la columna
 */
function getColumnByHeader(sheet: GoogleAppsScript.Spreadsheet.Sheet, columnName: string): number {
  const header = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0]
  return header.findIndex((element) => element == columnName) + 1
}
/**
 * Retorna un number que representa el número de la columna que se indica según el columnName
 * @param {any[][]} columnArray Array que solo tiene arrays de un solo largo
 * @param {any} value Valor a encontrar en la columna.
 */
function getRowByValue(columnArray: any[][] , value: any): number {
  return columnArray.findIndex(element => element[0] == value) + 1
}
/**
 * Le da estilo general: alto de fila, tamaño de la fuente, centra todo y le define margen arriba abajo y a los lados, entre medio no.
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango al cual colocarle estilo 
 * @param {{}} formatColumns Objeto que sus keys son los nombre de formato y contiene el número la columna que se le desea dar ese formato 
 */
function sStyle(range: GoogleAppsScript.Spreadsheet.Range, formatColumns: {
  date?: number,
  clp?: number,
  phone?: number,
  rut?: number}) {
  for (let format in formatColumns) {
    range.getCell(1, formatColumns[format]).setNumberFormat(formatForCells[format])
  }
  range.getSheet().setRowHeight(range.getRow(), 40)
  range = range.getSheet().getRange(range.getRow(), 1, 1, range.getSheet().getLastColumn())
  range.setFontSize(11).setHorizontalAlignment('center').setVerticalAlignment('middle')
    .setBorder(true, true, true, true, false, true).setWrap(true)
}
/**
 * Actualiza (le suma 1) y retorna el id del key dado
 * @param {string} key Llave de la propiedad 
 */
function getId(key: string): number{
  const value = Number(PropertiesService.getScriptProperties().getProperty(key)) + 1
  PropertiesService.getScriptProperties().setProperty(key, String(value))
  return value
}
/**
 * Coloca el archivo que se le entrega en la carpeta correspondiente según la fecha.
 * @param {GoogleAppsScript.Drive.Folder} file 
 * @param {GoogleAppsScript.Drive.File} folder Esta carpeta debe tener carpetas con los años. Cada carpeta con los años debe tener carpetas con los meses con esta estructura: yyyy-mm nameMesInUpperCase
 * @param {Date} date
 */
function putFileInMonthFolder(file: GoogleAppsScript.Drive.File, folder: GoogleAppsScript.Drive.Folder, date: Date): Boolean{
  const dateObj = dateToObject(date) 
  let folders = folder.getFolders()
  let folderDestiny: GoogleAppsScript.Drive.Folder;
  while (folders.hasNext()){
    folderDestiny = folders.next()
    if (dateObj.year == folderDestiny.getName()){
      folders = folderDestiny.getFolders()
      while(folders.hasNext()){
        folderDestiny = folders.next()
        if (dateObj.month == folderDestiny.getName()){
          folderDestiny.addFile(file)
          return true
        }
      }
    }
  }
  return false
}
/**
 * Borra la entrada representada con el id en la hoja entregada (s). Si no lo encuentra retorna false
 * @param {GoogleAppsScript.Spreadsheet.Sheet} s Hoja donde se aloja el supuesto id
 * @param {string} id Representa la entrada a borrar
 */
function deleteEntry(s: GoogleAppsScript.Spreadsheet.Sheet, id: string): boolean{
  let idColumn = getColumnByHeader(s, 'Id')
  let range = s.getRange(2, idColumn, s.getLastRow() - 1).getValues
  for (let i = 0; i < range.length; i++){
    if (id == range[i][0]){
      s.deleteRow(i + 2)
      return true
    }
  }
  return false
}