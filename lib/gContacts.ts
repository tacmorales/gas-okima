/**
 * Crea un nuevo contacto en Google Contacts según los parametros entregados en el objeto c, y lo asigna al grupo contactGroup de contactos
 * @param {{nombre: string, apellido: string, fono: string, correo:string, empresaOficio: string, direccion: string, rutFact: string}} c Objeto con los valores a colocar en el contacto
 * @param {string} contactGroup Grupo de contacto al que pertenece
 */
function addContact(c: {nombre: string, apellido: string, fono: string, correo:string, empresaOficio: string, direccion: string, rutFact: string}, contactGroup: string){
  const specificGroup = ContactsApp.getContactGroup(contactGroup)
  const allContacts = ContactsApp.getContactGroup("System Group: My Contacts")
  const contact = ContactsApp.createContact(c.nombre, c.apellido, c.correo)
  Utilities.sleep(3000)
  if (!c.correo){ contact.addEmail("Personal", "No definido") }
  const telefono = c.fono ? c.fono : "No definido"
  const empresaOficio = c.empresaOficio ? c.empresaOficio : "No definido"
  const direccion = c.direccion ? c.direccion : "No definido"
  const rutFact = c.rutFact ? c.rutFact : "No definido"
  contact.addCompany(empresaOficio, '')
  contact.addAddress("Principal", direccion)
  contact.addPhone("Principal", telefono)
  contact.addCustomField("RUT", rutFact)
  specificGroup.addContact(contact)
  allContacts.addContact(contact)
  const id = contact.getId()
  return id.substr(id.lastIndexOf("/") + 1)
}

/**
 * 
 * @param range Cliente actualizado, además de aqui se recupera el idGC
 * @param c Objeto con los datos a actualizar
 */
function editContact(id: string, c: {nombre: string, apellido: string, fono: string, correo:string, empresaOficio: string, direccion: string, rutFact: string}){
  let contacts = ContactsApp.getContacts()
  let contact;
  let idTrue;
  for (var i in contacts){
    contact = contacts[i]
    id = contact.getId()
    id = id.substr(id.lastIndexOf("/") + 1)
    if (id === idTrue){
      break
    }
  }
  if (id === idTrue){
    contact.setGivenName(c.nombre).setFamilyName(c.apellido)
    contact.getEmails()[0].setAddress(c.correo)   
    contact.getCompanies()[0].setCompanyName(c.empresaOficio)
    contact.getAddresses("Principal")[0].setAddress(c.direccion)
    contact.getPhones()[0].setPhoneNumber(c.fono)
    contact.getCustomFields("RUT")[0].setValue(c.rutFact)
  }
  else{
    console.log("El id que se dio a 'editContact()' no corresponde a ningún id de cliente en gc")
  }
}

/**
 * Transforma el event object a objeto que representa los datos de contacto. Ya sea una edicion o una entrada nueva.
 * @param {GoogleAppsScript.Events.SheetsOnFormSubmit} e 
 * @param {{nombre: string, apellido: string, fono: string, correo:string, empresaOficio: string, direccion: string, rutFact: string}} pairKeys
 */
function eventToContactObject(e: GoogleAppsScript.Events.SheetsOnFormSubmit, keyPairs: {nombre: string, apellido: string, fono: string, correo:string, empresaOficio: string, direccion: string, rutFact: string}){
  const s = e.range.getSheet()
  const row = e.range.getRow()
  const values = e.namedValues
  const nameSs = s.getRange(row, getColumnByHeader(s, keyPairs.nombre)).getValue()
  const nameForm = values[keyPairs.nombre][0]
  const apellidoSs = s.getRange(row, getColumnByHeader(s, keyPairs.apellido)).getValue()
  const apellidoForm = values[keyPairs.apellido][0]
  const correoSs = s.getRange(row, getColumnByHeader(s, keyPairs.correo)).getValue()
  const correoForm = values[keyPairs.correo][0]
  const telefonoSs = s.getRange(row, getColumnByHeader(s, keyPairs.fono)).getValue()
  const telefonoForm = values[keyPairs.fono][0]
  const empresaSs = s.getRange(row, getColumnByHeader(s, keyPairs.empresaOficio)).getValue()
  const empresaForm = values[keyPairs.empresaOficio][0]
  const direccionSs = s.getRange(row, getColumnByHeader(s, keyPairs.direccion)).getValue()
  const direccionForm = values[keyPairs.direccion][0]
  const rutSs = s.getRange(row, getColumnByHeader(s, keyPairs.rutFact)).getValue()
  const rutForm = values[keyPairs.rutFact][0]
  const c = {}
  c['nombre'] = nameForm ? nameForm : (nameSs ? nameSs : "No definido")
  c['apellido'] = apellidoForm ? apellidoForm : (apellidoSs ? apellidoSs : "No definido")
  c['correo'] = correoForm ? correoForm : (correoSs ? correoSs : "No definido")
  c['fono'] = telefonoForm ? telefonoForm : (telefonoSs ? telefonoSs : "No definido")
  c['empresaOficio'] = empresaForm ? empresaForm : (empresaSs ? empresaSs : "No definido")
  c['direccion'] = direccionForm ? direccionForm : (direccionSs ? direccionSs : "No definido")
  c['rutFact'] = rutForm ? rutForm : (rutSs ? rutSs : "No definido")
  return c
}