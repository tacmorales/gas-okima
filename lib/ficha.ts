/**
 * Crea la ficha personal (ss) del cliente. Le asigna nombre al spreadsheet y fija el link para editarlo.
 * @param {GoogleAppsScript.Spreadsheet.Range} range Entrada a la que se le crea ficha
 * @param {string} editResponseUrl Link para editar la entrada
 * @param {GoogleAppsScript.Drive.Folder} where Carpeta donde va el archivo
 * @param {GoogleAppsScript.Drive.File} template Archivo template
 * @param {string} nameSs Nombre a ponerle al Spreadsheet
 * @param {string} nameS Nombre donde van los datos de la entrada
 */
function createFile(range: GoogleAppsScript.Spreadsheet.Range, editResponseUrl: string, where: GoogleAppsScript.Drive.Folder, template: GoogleAppsScript.Drive.File, nameSs: string, nameS: string){
  const newFile = template.makeCopy(nameSs, where)
  const sheet = SpreadsheetApp.open(newFile).getSheetByName(nameS)
  sheet.getRange(1, 3).setValue(`=HYPERLINK(\\"${editResponseUrl}\\";\\"Editar a ${name}\\")`)
  const url = sheet.getParent().getUrl()
  const row = range.getRow()
  const valuesLength = range.getValues().length
   /* parte del 1. se obvia el 0 porque el primer valor es la marca temporal que no nos interesa colocar en las fichas */
  for (let i = 1; i < valuesLength; i++){ 
    let letter = (i + 1 + 9).toString(36).toUpperCase()
    let cell = `${nameS}!${letter}${row}`
    sheet.getRange(i, 2).setValue(`=IMPORTRANGE(\\"${url}\\";\\"${cell}\\")`)
  }
  return newFile;
}