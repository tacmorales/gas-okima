/**
 * Crea el reporte de ingresos diarios
 */
function createDailyReportIn() {
  const date = new Date()
  const nameDailyReportIn = `${dateToStringSortableDate(date)} | ingresos`
  const checkFile = DriveApp.getFilesByName(nameDailyReportIn)
  if (checkFile.hasNext()){
    console.log(`Archivo con el nombre ${nameDailyReportIn} ya existe. Se detuvo la ejecución.`)
    return
  }
  const plantillaReportInFile = DriveApp.getFileById(idSSheets['ingresosReportTemplate'])
  const dailyReportInFile = plantillaReportInFile.makeCopy(nameDailyReportIn)
  if (!putFileInMonthFolder(dailyReportInFile, DriveApp.getFolderById(idFolder["inReports20"]), date)){
    console.log(`Archivo con el nombre ${nameDailyReportIn} no se pudo colocar en su carpeta respectiva. Actualmente solo está en la carpeta 'Plantilla reportes'. No se detuvo la ejecución`)
  }
  const dailyReportInSs = SpreadsheetApp.open(dailyReportInFile)
  setResumen(dailyReportInSs, dateToObject(date))
}
/**
 * 
 */
function createMonthReports(){
  const date = new Date()
  const dateObj = dateToObject(date)
  const nameMonthReportIn = `${dateObj.year}-${dateObj.month} | ${dateObj.weekDay} | ingresos`
  const checkFile = DriveApp.getFilesByName(nameMonthReportIn)
  if (checkFile.hasNext()){
    console.log(`Archivo con el nombre ${nameMonthReportIn} ya existe. Se detuvo la ejecución.`)
    return
  }
  const plantillaReportInFile = DriveApp.getFileById(idSSheets['ingresosReportTemplate'])
  const monthReportInFile = plantillaReportInFile.makeCopy(nameMonthReportIn)
  if (putFileInMonthFolder(monthReportInFile, DriveApp.getFolderById(idFolder["inReports20"]), date)){
    console.log(`Archivo con el nombre ${nameMonthReportIn} no se pudo colocar en su carpeta respectiva. Actualmente solo está en la carpeta 'Plantilla reportes'. No se detuvo la ejecución`)
  }
}
//Fija el resumen del día en la hoja anual
function setResumen(ssDailyReport, dateObj) {
  let ano = dateObj.year
  let mes = dateObj.month
  let dia = dateObj.day
  let ssPagos = SpreadsheetApp.openById(idSSheets['pagosAnual']) 
  let ssIngresos = SpreadsheetApp.openById(idSSheets['ingresosAnual']) 
  let diaPago = ssPagos.getSheetByName("Total Pagos").getRange(Number(dia) + 1, Number(mes) + 1)
  let isFeriado = diaPago.getValue() === "Feriado"
  let isDomingo = diaPago.getValue() === "Domingo"
  let color = isDomingo ? "#cccccc" : (isFeriado ? "#ffd052" : "#ffffff")
  //Pagos
  setInAnual(ssDailyReport, "Resumen", 3, 2, ssPagos, "Efectivo", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 3, ssPagos, "Tarjeta Debito", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 4, ssPagos, "Tarjeta Credito", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 5, ssPagos, "Transferencia", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 6, ssPagos, "Cheque", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 7, ssPagos, "Groupon", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 3, 8, ssPagos, "Total Pagos", dia, mes, color)
  //Ingresos
  setInAnual(ssDailyReport, "Resumen", 7, 2, ssIngresos, "Efectivo", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 3, ssIngresos, "Tarjeta Debito", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 4, ssIngresos, "Tarjeta Credito", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 5, ssIngresos, "Transferencia", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 6, ssIngresos, "Cheque", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 7, ssIngresos, "Groupon", dia, mes, color)
  setInAnual(ssDailyReport, "Resumen", 7, 8, ssIngresos, "Total Ingresos", dia, mes, color)
}

/*
 *
 * FUNCIONES AUXILIARES
 *
 */
 
//Casi no es auxiliar. Hace lo más importante, pero era tan reptitivo. 
//Coloca el valor de origin en la hoja destiny, dependiendo de la fecha. Sincronizado con importRange
function setInAnual(ssOrigin, nameSheetOrigin, rowOrigin, columnOrigin, ssDestiny, nameSheetDestiny, dia, mes, color){
  let rangeA1Notation =  ssOrigin.getSheetByName(nameSheetOrigin).getRange(rowOrigin, columnOrigin).getA1Notation()
  let valor = 'IMPORTRANGE("' + ssOrigin.getUrl() + '";"' + nameSheetOrigin + '!' + rangeA1Notation + '")'
  let cellToUpdate = ssDestiny.getSheetByName(nameSheetDestiny).getRange(Number(dia) + 1, Number(mes) + 1)
  .setBackground(color).setFontLine("none").setFontColor("#000000")
  cellToUpdate.setValue('=HYPERLINK("'+ssOrigin.getUrl()+'";'+valor+')')
}

function yul(){
  var files = DriveApp.getFolderById(id["folderDailyReport03"]).getFiles()
  var file
  var s = SpreadsheetApp.openById("1AWRGbtiGR5Gks5QGrF0UF5dPUyHIog1PMTVIWhhq1zg")
  var sheetToCopy1 = s.getSheetByName("Ordenes")
  var sheetToCopy2 = s.getSheetByName("Pagos")
  while (files.hasNext()){
    file = files.next()
    var ss = SpreadsheetApp.open(file)
    var sheetToDelete1 = ss.getSheetByName("Ordenes")
    var sheetToDelete2 = ss.getSheetByName("Pagos")
    ss.deleteSheet(sheetToDelete1)
    ss.deleteSheet(sheetToDelete2)
    sheetToCopy1.copyTo(ss).setName("Ordenes")
    sheetToCopy2.copyTo(ss).setName("Pagos")
  }  
}

/*Ejecuta setResumen según los datos que se ingresen (nameFolder es el mes, y nameFile es el archivo ensi)
function yei(){
  let nameFolder = ""; //folderDailyReportXX
  let nameFile = ""; //2020-03-17 | Martes
  let files = DriveApp.getFolderById(id[nameFolder]).getFilesByName(nameFile);
  let file = files.next();
  file = SpreadsheetApp.open(file);
  setResumen(file);
}

function yum(){
  var files = DriveApp.getFolderById(id["folderDailyReport03"]).getFiles();
  var file;
  var sheetToCopy = SpreadsheetApp.openById("1AWRGbtiGR5Gks5QGrF0UF5dPUyHIog1PMTVIWhhq1zg").getSheetByName("Pagos");
  while (files.hasNext()){
    file = files.next();
    var ss = SpreadsheetApp.open(file);
    ss.getSheetByName("Cierre caja").setName("Resumen pagos").deleteRow(5).deleteRow(3);
    ss.getSheetByName("Ingreso Real").setName("Resumen ingresos");
    ss.getSheetByName("Pagos").setName("Pagos a la antigua");
    sheetToCopy.copyTo(ss).setName("Pagos");
    setResumen(ss);
  }  
}
*/