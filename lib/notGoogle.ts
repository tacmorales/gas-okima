/**
 * Retorna un string que representa la fecha pero en y/m/d | weekDay
 * @param {string} strDate Fecha en el formato: d/m/y
 */
function strDateToStrReadableDate(strDate: string): string {
  const dateObj = strDateToObject(strDate)
  return dateObj.weekDay + ' | ' + dateObj.day + '-' + dateObj.month + '-' + dateObj.year
}
/**
 * Retorna un string que representa la fecha pero en y/m/d | weekDay
 * @param {string} strDate 
 */
function strDateToStrSortableDate(strDate): string {
  const dateObj = strDateToObject(strDate)
  return dateObj.year + '-' + dateObj.month + '-' + dateObj.day + ' | ' + dateObj.weekDay
}
/**
 * Retorna un object que representa la fecha dividida en atributos
 * @param {string} strDate e.namedValues["Fecha"][0]: String with this format: 5/04/2020 (Months always 2 digit. Day 1 or 2 digit)
 */
function strDateToObject(strDate): {weekDay: string, day: string, month: string, year: string} {
  const length = strDate.length
  const year = strDate.substring(length - 4)
  const month = strDate.substring(length - 7, length - 5)
  let day = strDate.substring(length - 10, length - 8)
  if (day.length === 1) {
    day = new String('0' + day)
  }
  const weekDay = diasWeek[new Date(year + '/' + month + '/' + day).getDay()]
  return {
    weekDay: weekDay,
    day: day,
    month: month,
    year: year
  }
}
/**
 * Retorna un object que representa la fecha dividida en atributos
 * @param {date} date Fecha en el formato: d/m/y
 */
function dateToObject(date): {weekDay: string, day: string, month: string, year: string} {
  let dia = String(date.getDate());
  let mes = String(date.getMonth() + 1);
  const ano = String(date.getFullYear());
  if (dia.length === 1) { dia = String('0' + dia); }
  if (mes.length === 1) { mes = String('0' + mes); }
  var diaSemana = diasWeek[new Date(ano + '/' + mes + '/' + dia).getDay()]
  return {
    weekDay: diaSemana,
    day: dia,
    month: mes,
    year: ano
  }
}
/**
 * Reemplaza la última letra que representa la columna de la segunda celda (segundo punto)
 * @param {string} A1Notation 
 * @param {string} endColumn
 */
function remplaceEndInA1N(A1Notation: string, endColumn: number): string {
  let end;
  if (endColumn < 27) {
    end = (endColumn + 9).toString(36).toUpperCase()
  } else if (endColumn < 52) { //works until 'AY' o 51 columnas
    end = (Math.floor(endColumn / 26) + 9).toString(36).toUpperCase() + (endColumn % 26 + 9).toString(36).toUpperCase()
  } else {
    console.log('entryForImport solo procesa hasta 51 columnas :(')
    return
  }
  const start = A1Notation.indexOf(':') + 1
  const endCell = A1Notation.substr(start)
  const length = endCell.substr(0, indexOfFirstDigit(endCell)).length
  return A1Notation.substr(0, start) + endColumn + A1Notation.substr(start + length)
}
/**
 * Retorna el el index de la primera aparición de un número en el string input
 * @param {string} input Texto donde se busca el index de la primera aparicion de un numero
 */
function indexOfFirstDigit(input): number {
  let i = 0;
  for (; input[i] < '0' || input[i] > '9'; i++);
  return i == input.length ? -1 : i;
}
/**
* Retorna un string que representa la fecha pero en y/m/d | weekDay
* @param {Date} date Fecha
*/
function dateToStringSortableDate(date){
  const dateObj = dateToObject(date)
  return dateObj.year + '-' + dateObj.month + '-' + dateObj.day + ' | ' + dateObj.weekDay
}

/**
  * Retorna un string que representa la fecha pero en weekDay | y/m/d 
  * @param {Date} date Fecha
 function dateToStringReadableDate(date){
   var dia = new String(date.getDate());
   var mes = new String(date.getMonth() + 1);
   var ano = new String(date.getFullYear());
   if (dia.length === 1) { dia = new String('0' + dia); }
   if (mes.length === 1) { mes = new String('0' + mes); }
   var fecha = dia + '-' + mes + '-' + ano;
   var diaSemana = diasWeek[new Date(ano + '/' + mes + '/' + dia).getDay()]
   return diaSemana + ' | ' + fecha;
 }
 */