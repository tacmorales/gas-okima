/**
 * Create y Update para TODAS las entidades: EditionsCount, Id (solo la primera vez), EditUrl (solo la primera vez) y ÚltimaEdicionPor
 * @param {GoogleAppsScript.Events.SheetsOnFormSubmit} e 
 */
function formSubmitAlways(e: GoogleAppsScript.Events.SheetsOnFormSubmit, nameEntrys: string = e.range.getSheet().getName().toLowerCase()): number{
  const range = e.range
  const sheet = range.getSheet()
  const numEdiciones = setEditionsCount(range)
  if (!numEdiciones){
    setValueInEntry(range, 'Id', getId(nameEntrys))
    const timestamp = range.getCell(1, getColumnByHeader(sheet, 'Marca temporal')).getValue();
    setEditUrl(range, timestamp, nameEntrys)
    //Put original entry in tab 'Entradas sin edicion'
  }
  setValueInEntry(range, 'Última edición por', e['Antes que todo ¿Quien eres?'])
  return numEdiciones
}
/**
 * Create y Update para los pagos: crear informe
 * @param {GoogleAppsScript.Events.SheetsOnFormSubmit} e 
 */
function formSubmitPagos(e) {
  const numEdiciones = formSubmitAlways(e.range)
  const columnDate = getColumnByHeader(e.range.getSheet(), 'Fecha')
  const columnClp = getColumnByHeader(e.range.getSheet(), 'Monto')
  const formatColumns = {
    date: columnDate,
    clp: columnClp
  }
  const timestamp = e.range.getCell(1, getColumnByHeader(e.range.getSheet(), 'Marca temporal')).getValue();
  const fecha : string = e.namedValues['Fecha'][0];
  let ssObject = {folder: 'clientes', name: ''}
  let range = e.range;
  if (!numEdiciones) {
    ssObject.folder = e.namedValues['Cliente'][0]
    range = setEntryInSs(e.range, ssObject)
    sStyle(range, formatColumns)
    const month = fecha.substring(fecha.indexOf('/') + 1, fecha.indexOf('/') + 3)
    const year = fecha.substring(fecha.lastIndexOf('/') + 1, fecha.lastIndexOf('/') + 5)
    ssObject.folder = 'inReport' + month + year
    ssObject.name = strDateToStrSortableDate(e.namedValues['Fecha'][0])
    setEntryInSs(e.range, ssObject)
    sStyle(range, formatColumns)
  } else if (numEdiciones === 1){
    // create file ficha
  }
  else{
    if (e.namedValues['Cliente'][0]) {
      const oldClient = getOldValue(e.range, 'Cliente');
      ssObject['name'] = oldClient
      deleteEntryInSs(e.range, oldClient, timestamp);
      ssObject['name'] = e.namedValues['Cliente'][0]
      range = setEntryInSs(e.range, ssObject);
      sStyle(range, formatColumns)
    }
    /*
    if (e.namedValues['Fecha'][0]) {
        const oldDate = new Date(getOldValue(e.range, 'Fecha'));
        deleteEntryInSs(e.range, oldDate, timestamp); //////////
        setEntryInSs(e.range, strDateToStrSortableDate(e.namedValues['Fecha'][0]), 3, 6);
    }
    */
  }
  sStyle(range, formatColumns)
}
/**
 * Full implementación. Agrega editionUrl, contador de ediciones y le da formato. Luego coloca la entrada en la ficha del cliente.
 * La quita del cliente antiguo y la coloca en el cliente nuevo si hay una edición del pago
 * Sirve para pagos y ordenes por ahora.
 * @param {Event} e Objeto evento de donde viene la respuesta
 * @param {number} columnDate Numero que representa la columna donde va el formato fecha
 * @param {{}} formatColumns Objeto que sus keys son los nombre de formato y contiene el número la columna que se le desea dar ese formato 
 */
/*
function formSubmitToSs(e: GoogleAppsScript.Events.SheetsOnFormSubmit, formatColumns: {
  date?: number,
  clp?: number,
  phone?: number,
  rut?: number}) {
  const numEdiciones = setEditionsCount(e.range)
  const timestamp = e.range.getCell(1, 1).getValue();
  //const fecha = e.namedValues['Fecha'][0];
  let ssObject = {folder: 'clientes', name: ''}
  let range = e.range;
  if (!numEdiciones) {
    let nameEntrys = pluralToSingular[range.getSheet().getParent().getName().toLowerCase()]
    setEditUrl(e.range, timestamp, nameEntrys)
    ssObject['folder'] = 'clientes'
    ssObject['name'] = e.namedValues['Cliente'][0]
    range = setEntryInSs(e.range, ssObject)
    sStyle(range, formatColumns)
    /*
    const month = fecha.substring(fecha.indexOf('/') + 1, fecha.indexOf('/') + 3)
    const year = ...
    ssObject.folder = 'dailyReport' + month + year
    ssObject.name = strDateToStrSortableDate(e.namedValues['Fecha'][0])
    setEntryInSs(e.range, ssObject)
    sStyleDateClp(range, columnDate, columnClp)
    */
   /*
  } else {
    if (e.namedValues['Cliente'][0]) {
      const oldClient = getOldValue(e.range, 'Cliente');
      ssObject['name'] = oldClient
      deleteEntryInSs(e.range, oldClient, timestamp);
      ssObject['name'] = e.namedValues['Cliente'][0]
      range = setEntryInSs(e.range, ssObject);
      sStyle(range, formatColumns)
    }
    /*
    if (e.namedValues['Fecha'][0]) {
        const oldDate = new Date(getOldValue(e.range, 'Fecha'));
        deleteEntryInSs(e.range, oldDate, timestamp); //////////
        setEntryInSs(e.range, strDateToStrSortableDate(e.namedValues['Fecha'][0]), 3, 6);
    }
    */
   /*
  }
  /*
  sStyle(range, formatColumns)
}
/**
 * Agrega editionUrl, contador de ediciones y le da formato a la entrada. Además crea un ss en el que se puede ver más claro la info del cliente lugar donde se irán agregando sus ordenes y sus pagos. Además agrega el contacto a GoogleContacts.
 * @param {Event} e Objeto evento de donde viene la respuesta
 */
/*
function formSubmitToSsFicha(e: GoogleAppsScript.Events.SheetsOnFormSubmit, shortNameEntry: string, longNameEntry: string, idEntry, folder, shouldUpdateForms: boolean) {
  const numEdits = setEditionsCount(e.range)
  const timestamp = e.range.getCell(1, 1).getValue();
  //const fecha = e.namedValues['Fecha'][0];
  const sheet = e.range.getSheet()
  const nameEntrys = e.range.getSheet().getName()
  let range;
  if (shouldUpdateForms){
    updateFormCliente(sheet)
  }
  if (!numEdits) {
    setValueInEntry(e.range, 'Id', getId(nameEntrys))
    const editResponseUrl = setEditUrl(e.range, timestamp, shortNameEntry);
    const template = DriveApp.getFileById(idSSheets['template' + nameEntrys])
    const folderForFicha = DriveApp.getFolderById(idFolder[nameEntrys.toLowerCase()])
    const nameFicha = longNameEntry
    const ficha = createFile(e.range, editResponseUrl, folderForFicha, template, nameFicha, 'Datos '+ pluralToSingular[nameEntrys.toLowerCase()])
    setValueInEntry(e.range, 'Ver detalles', '=HYPERLINK("' + ficha.getUrl() + '";"Ver detalles de ' + shortNameEntry + '")')
    let idGC = addContact(e.namedValues)
    setValueInEntry(e.range, 'Id google contacts', idGC)
  } else {
    updateContact(e)
  }
  sStylePhoneRut(e.range, 6, 10)
}
/**
 * Actualiza el form particularmente para el tipo Cliente. Solo existe para DRY
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet La hoja para poder recuperar los nombres de los clientes
 */
/*
function updateFormCliente(sheet: GoogleAppsScript.Spreadsheet.Sheet){
  const formsAndQns = [{
    idForm: idForms['pagos'],
    idQn: idQuestions['pagosCliente']
  }, {
    idForm: idForms['ordenes'],
    idQn: idQuestions['ordenesCliente']
  }]
  const columnCliente = getColumnByHeader(sheet, 'Cliente')
  updateForms(formsAndQns, sheet.getRange(1, columnCliente, sheet.getLastRow()).getValues())
}*/
