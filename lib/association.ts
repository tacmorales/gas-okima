/**
 * Cambia la asociación de la entrada
 * @param {string} id Representa la entrada que se desea borrar
 * @param {string} entityId Entidad de la id
 * @param {string} entityAsociation Entidad de la asociación
 * @param {string} oldIdValue Antiguo valor a desasociar
 * @param {string} newIdValue Nuevo valor a asociar
 */
function changeAssociation(id: string, entityId: string, entityAsociation: string, oldValue: string, newValue: string){

  const folder = DriveApp.getFolderById(idFolder[ssObject.folder])
  const file = getFileByName(ssObject.value, folder)
  if (!file){
    console.log('Fail in deleteEntryInSs: no se encuentra el archivo')
    return
  }
  const nameEntry = range.getSheet().getParent().getName()
  const sheet = SpreadsheetApp.open(file).getSheetByName(nameEntry)
  const columnArray = sheet.getRange(1, 1, sheet.getLastRow()).getValues()
  const row = getRowByValue(columnArray, timestamp)
  sheet.deleteRow(row)
}

/**
  * Put la entry al sS (informe) representado en ss
  * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que se quiere exportar desde nameEntry / importar hacia value.value
  * @param {string} entityAsociation folder: (Clientes | DailyReportXX), name: ({Nombre del cliente} | {SortableDate}) Ambos string
  * @param {string} idValue
  */
 function setAssociation(range: GoogleAppsScript.Spreadsheet.Range, entityAsociation: string, idValue: string): GoogleAppsScript.Spreadsheet.Range{
  const folder = DriveApp.getFolderById(idFolder[ssObject.folder])
  const file = getFileByName(ssObject.name, folder)
  if (!file){
    console.log('Fail in setEntryInSs->getFileByName')
    return
  }
  const newRow = entryForImport(range)
  const nameEntry = range.getSheet().getParent().getName()
  const sheet = SpreadsheetApp.open(file).getSheetByName(nameEntry)
  sheet.appendRow([newRow])
  return sheet.getRange(sheet.getLastRow(), 1, 1, sheet.getLastColumn())
}

/**
 * Cambia la asociación de la entrada
 * @param {string} id Representa la entrada que se desea borrar
 * @param {string} entityId Entidad de la id
 * @param {string} entityAsociation Entidad de la asociación
 * @param {string} oldIdValue Valor a desasociar
 */
 function deleteAssociation(range: GoogleAppsScript.Spreadsheet.Range, entityAsociation: string, idValue: string): GoogleAppsScript.Spreadsheet.Range{
  const folder = DriveApp.getFolderById(idFolder[ssObject.folder])
  const file = getFileByName(ssObject.name, folder)
  if (!file){
    console.log('Fail in setEntryInSs->getFileByName')
    return
  }
  const newRow = entryForImport(range)
  const nameEntry = range.getSheet().getParent().getName()
  const sheet = SpreadsheetApp.open(file).getSheetByName(nameEntry)
  sheet.appendRow([newRow])
  return sheet.getRange(sheet.getLastRow(), 1, 1, sheet.getLastColumn())
}

/**
 * 
 * @param {{ idForm: string, idQn: number; }[]} formsAndQuestions 
 * @param {Array<string[]>} values 
 */
function updateForms(formsAndQns: { idForm: string, idQn: number; }[], values: string[][]){
  let arrayOfChoices = []
  for (let i = 0; i < formsAndQns.length; i++){
    let qn = FormApp.openById(formsAndQns[i].idForm).getItemById(formsAndQns[i].idQn).asListItem()
    arrayOfChoices.push([])
    for (let j = 0; j < values.length; j++){
      arrayOfChoices[i].push(values[i])
    }
    qn.setChoices(arrayOfChoices[i])
  }
}

function rangeEntryToObject(range: GoogleAppsScript.Spreadsheet.Range){
  const nameEntryPlural = range.getSheet().getName()
  const nameEntrySingular = pluralToSingular[nameEntryPlural]
  const idEntry = range.getCell(1,1).getValue()
  const strForImport = entryForImport(range)
  const ss = range.getSheet().getParent()
  const folder = idFolder[nameEntryPlural]
}

/**
 * Retorna un string formula para exportar el rango que otro spreadsheet necesita importar. El rango debe ser de solo una fila.
 * @param {GoogleAppsScript.Spreadsheet.Range} range Rango que representa la entrada
 */
function entryForImport(range: GoogleAppsScript.Spreadsheet.Range): string {
  const sheet = range.getSheet()
  const numOfColumn = sheet.getLastColumn()
  let A1Notation = range.getA1Notation()
  A1Notation = remplaceEndInA1N(A1Notation, numOfColumn)
  return '=IMPORTRANGE("' + sheet.getParent().getUrl() + '";"' + sheet.getName() + '!' + A1Notation + '")'
}