const cellSets ={
  1: {
    headerColor1 : '#A4C2F4',
    headerColor : '#f7cb4d',
    firstAndSecondColorFirst1 : '#F1C232',
    firstColor : '#FFFFFF',
    secondColor : '#FFF2CC',
    cellFormat : '"$"#,##0',
  },
  2: {
    headerColor1 : '#A4C2F4',
    headerColor : '#660000',
    firstAndSecondColorFirst1 : '#660000',
    firstColor : '#FFFFFF',
    secondColor : '#F4CCCC',
    cellFormat : '"$"#,##0" c/u"',
  }
}
/**
 * Crea y le da formato adecuado para una tabla de precios bella
 */
function createTable() { 
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  let s = ss.getActiveSheet()
  const sName = s.getName()
  let version;
  if (sName == 'AnalizarRangos: PrecioFijoPorRango'){
    version = 1
  } else if (sName == 'AnalizarRangos: PrecioUnitarioPorRango'){
    version = 2
  } else{
    SpreadsheetApp.getUi().alert('no se genera tabla porque no estas en una hoja tipo AnalizarRangos')
    return
  }

  //Get values and versionOfColorsAndNumberFormat from sheet source
  let lastColumn = s.getLastColumn()
  let lastRow = s.getLastRow()
  const product = s.getRange(1,1).getValue()
  const rangos = createRango(s.getRange(1, 3, 2, lastColumn - 2).getValues())
  rangos.unshift(product) 
  const values: Array<Array<any>> = [rangos]
  for (let i = 3; i < s.getLastRow(); i += 3){
    let variant = s.getRange(i, 1, 1, lastColumn).getValues()[0]
    variant.splice(1,1)
    values.push(variant)
  }

  //Create new sheet and give format
  s = ss.insertSheet(product, 0)
  const columnsDifference = s.getLastColumn() - lastColumn
  const rowDifference = s.getLastRow() - lastRow
  setSizeSheet(s, columnsDifference, rowDifference)
  lastColumn = s.getLastColumn()
  lastRow = s.getLastRow()
  setStyle(s, version, lastRow, lastColumn)

  //put values
  s.getRange(1,1,lastRow,lastColumn).setValues(values)
}
/**
 * Fija los estilos según la versión entregada. Banding y formatNumbers.
 * @param {GoogleAppsScript.Spreadsheet.Range} s 
 * @param {number} version versión de la tabla
 * @param {number} numRows número de filas de la sheet
 * @param {number} numColumns número de columnas de la sheet
 */
function setStyle(s: GoogleAppsScript.Spreadsheet.Sheet, version: number, numRows: number, numColumns: number){
  let range: GoogleAppsScript.Spreadsheet.Range;
  if (version == 1){
    s.getRange(1,1,numRows,1).applyRowBanding()
      .setHeaderRowColor(cellSets[version].headerColor1)
      .setFirstRowColor(cellSets[version].firstAndSecondColorFirst1)
      .setSecondRowColor(cellSets[version].firstAndSecondColorFirst1)
    s.getRange(1,2,numRows,numColumns - 2).applyRowBanding()
      .setHeaderRowColor(cellSets[version].headerColor)
      .setFirstRowColor(cellSets[version].firstColor)
      .setSecondRowColor(cellSets[version].secondColor)
    s.getRange(1,numColumns,numRows,1).applyRowBanding()
      .setHeaderRowColor(cellSets[version+1].headerColor)
      .setFirstRowColor(cellSets[version+1].firstColor)
      .setSecondRowColor(cellSets[version+1].secondColor)
    range = s.getRange(2, 2, numRows - 1, numColumns - 2)
    range.setNumberFormat(cellSets[version].cellFormat)
    range = s.getRange(2, numColumns, numRows - 1)
    range.setNumberFormat(cellSets[version + 1].cellFormat)
  }
  else{
    range = s.getRange(2,2,numRows - 1, numColumns - 1)
    range.setNumberFormat(cellSets[version].cellFormat)
  }
  s.getRange(1,1,numRows,numColumns).setFontSize(14)
  s.getRange(1,2,1,numColumns-1).setFontSize(16)
}
/**
 * Según colorsets aplica el subtitleColor, 
 * @param {GoogleAppsScript.Spreadsheet.Range} range banding to edit
 * @param {number} version del color 
 * @param {boolan} isMain true: se aplica verisón principal, false: se palica verisón light 
 */
function setBanding(range: GoogleAppsScript.Spreadsheet.Range, version: number, isFirst: boolean){
  let subTitleColor
  let firstRowColor
  let secondRowColor
  if (isFirst){
    subTitleColor = cellSets[version].subTitleColor
    firstRowColor = cellSets[version].firstRowColor
    secondRowColor = cellSets[version].secondRowColor
  }
  else{
    subTitleColor = cellSets[version].subTitleColorFirst
    firstRowColor = cellSets[version].firstRowColorFirst
    secondRowColor = cellSets[version].secondRowColorFirst
  }
  range.applyColumnBanding()
    .setHeaderRowColor(subTitleColor)
    .setFirstRowColor(firstRowColor)
    .setSecondRowColor(secondRowColor)
}
/**
 * Set size in new sheet
 * @param {GoogleAppsScript.Spreadsheet.Sheet} s 
 * @param {number} columnsDifference 
 * @param {number} rowDifference 
 */
function setSizeSheet(s: GoogleAppsScript.Spreadsheet.Sheet, columnsDifference: number, rowDifference: number){
  if (!columnsDifference){}
  else if (columnsDifference < 0){
    columnsDifference = Math.abs(columnsDifference)
    s.insertColumns(1, columnsDifference)
  }
  else{ 
    s.deleteColumns(1, columnsDifference) 
  }
  if (!rowDifference){}
  else if (rowDifference < 0){
    rowDifference = Math.abs(rowDifference)
    s.insertRows(1, rowDifference)
  }
  else{ 
    s.deleteColumns(1, rowDifference) 
  }
}
/**
 * El array debe ser de 2xN. Une cada index de cada array unido por ' - '. Ej: valor1 - valor2
 * @param {[][]} array 
 * @returns {[]}
 */
function createRango(array){
  const arrayResult = []
  let i;
  for (i = 0; i < array[0].length; i++){
    arrayResult.push(array[0][i] + " - " + array[1][i])
  }
  arrayResult[i] = array[1][i] + ' o más'
  return arrayResult
}

/**
 * 
 * @param e 
 */
function onOpen(e: GoogleAppsScript.Events.SheetsOnOpen){
  const ui = SpreadsheetApp.getUi()
  ui.createMenu('crear').addItem('Tabla de precios', 'createTable').addToUi()
}